from elasticsearch import Elasticsearch
import configparser
import teams_trigrams_significant
import indiv_trigrams_significant

def get_temp_publication() :
    query = {
        "size" : 100,
        "query" : {"match_all" : {}}
    }
    return es.search(index=index_temp_pub, doc_type=doc_type_pub, body=query, scroll="20m")

def get_all_temp_publication() :
    query = {
    "query" : {"match_all" : {}}
    }
    return es.search(index=index_temp_pub, doc_type=doc_type_pub, body=query)

def delete_temp_pub(id_pub, body) :
    es.index(index=index_pub, doc_type=doc_type_pub, id=id_pub, body=body)
    es.delete(index=index_temp_pub, doc_type=doc_type_pub, id=id_pub)

def get_temp_publication_team(id_team) :
    query = {
    "query": {
        "nested": {
           "path": "affiliations",
           "query": {
               "match": {
                  "affiliations.id": id_team
               }
           }
        }
    }
    }
    return es.search(index=index_temp_pub, doc_type=doc_type_pub, body=query)

def get_temp_publication_user(name) :
    query = {
    "query": {
        "nested": {
           "path": "authors",
           "query": {
               "match": {
                  "authors.formatName": name_author
               }
           }
        }
    }
    }
    return es.search(index=index_temp_pub, doc_type=doc_type_pub, body=query)

def team_exist(id_team) :
    return es.get(index=index_team, doc_type=doc_type_team, id=id_team, ignore=[400, 404])

def user_exist(name_user) :
    query = {
        "query": {
            "match" : { "formatName" : name_user}
        }
    }
    return es.search(index=index_author, doc_type=doc_type_author, body=query)

def update_team(idteam, team) :
    es.update(index=index_team, doc_type=doc_type_team, id=idteam, body={"doc" :team}, request_timeout=30)

def update_indiv(id_author, indiv) :
    es.update(index=index_author, doc_type=doc_type_author, id=id_author, body={"doc" : indiv}, request_timeout=30)


def formatName(name) :
    name = name.lower()
    name = name.replace("é", "e")
    name = name.replace("è", "e")
    name = name.replace("ê", "e")
    name = name.replace("ë", "e")
    name = name.replace("à", "a")
    name = name.replace("ï", "i")
    name = name.replace("î", "i")
    name = name.replace("ô", "o")
    name = name.replace("ç", "c")
    name = name.replace("ü", "u")
    name = name.replace("ù", "u")
    name = name.replace("-", " ")
    return name

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))

index_pub = config.get("elasticsearch", "index_pub")
doc_type_pub = config.get("elasticsearch", "doc_type_pub")

index_temp_pub = config.get("elasticsearch", "temp_index_pub")

index_author = config.get("elasticsearch", "index_author")
doc_type_author = config.get("elasticsearch", "doc_type_author")

index_team = config.get("elasticsearch", "index_team")
doc_type_team = config.get("elasticsearch", "doc_type_team")

nbfound = get_all_temp_publication()["hits"]["total"]

author_done = []
team_done = []

nb_range = int(nbfound/100) + 1
for i in range(0, nb_range) :
    if i == 0 :
        pubs = get_temp_publication()
    else :
        pubs = es.scroll(scroll_id=scroll_id, scroll="20m")
    scroll_id = pubs["_scroll_id"]
    for pub in pubs["hits"]["hits"] :
        for author in pub["_source"]["authors"] :
            name_author = formatName(author["forename"] +" " +author["surname"])
            if name_author not in author_done :
                author_exist = user_exist(name_author)
                update_author = {}
                if len(author_exist["hits"]["hits"]) == 1 :
                    publications = get_temp_publication_user(name_author)
                    update_author["pubs"] = author_exist["hits"]["hits"][0]["_source"]["pubs"]
                    for pub_to_add in publications["hits"]["hits"] :
                        del pub_to_add["_source"]["affiliations"]
                        del pub_to_add["_source"]["projects"]
                        update_author["pubs"].append(pub_to_add["_source"])
                    new_main_topics = indiv_trigrams_significant.indiv_new_topics(author_exist["hits"]["hits"][0]["_source"]["formatName"])
                    update_author["main_topics"] = new_main_topics
                    update_indiv(author_exist["hits"]["hits"][0]["_id"], update_author)
                author_done.append(name_author)
        for team in pub["_source"]["affiliations"] :
            team_id = team["id"]
            if team_id not in team_done :
                is_team = team_exist(team_id)
                if is_team["found"] :
                    publications = get_temp_publication_team(team_id)
                    updateTeam = {}
                    updateTeam["pubs"] = is_team["_source"]["pubs"]
                    for pub_to_add in publications["hits"]["hits"] :
                        del pub_to_add["_source"]["affiliations"]
                        del pub_to_add["_source"]["projects"]
                        updateTeam["pubs"].append(pub_to_add["_source"])
                    new_main_topics = teams_trigrams_significant.teams_new_topics(is_team["_source"]["acronym"])
                    updateTeam["main_topics"] = new_main_topics
                    update_team(team_id, updateTeam)
                team_done.append(team_id)
        delete_temp_pub(pub["_id"], pub["_source"])
