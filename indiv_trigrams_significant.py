from elasticsearch import Elasticsearch
import configparser
import trigrams_with_significant_keywords

def get_keywordsResearcher(halId, es) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    index_pub = config.get("elasticsearch", "index_pub")
    doc_type_pub = config.get("elasticsearch", "doc_type_pub")
    query_keywords = {
                "aggregations": {
                    "all_pub": {
                        "global": {},
                        "aggs": {
                            "pub_filter": {
                                "filter": {
                                    "terms": {
                                        "halId": halId
                                    }
                                },
                                "aggs": {
                                    "most_sig_words": {
                                        "significant_terms": {
                                            "field": "title_abstract_sign",
                                            "size": 100
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
    }
    result = es.search(index=index_pub, doc_type=doc_type_pub, body=query_keywords)
    if "most_sig_words" in result["aggregations"]["all_pub"]["pub_filter"] :
        result = result["aggregations"]["all_pub"]["pub_filter"]["most_sig_words"]["buckets"]
    else :
        result = ""
    return result

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_author(formatName, es) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    index_author = config.get("elasticsearch", "index_author")
    doc_type_author = config.get("elasticsearch", "doc_type_author")
    query = {
        "query" : {"match" : {"formatName" : formatName}}
    }
    return es.search(index=index_author, doc_type=doc_type_author, body=query)


def indiv_new_topics(formatName) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    es = Elasticsearch(config.get("elasticsearch", "ip"))
    index_author = config.get("elasticsearch", "index_author")
    doc_type_author = config.get("elasticsearch", "doc_type_author")
    index_pub = config.get("elasticsearch", "index_pub")
    doc_type_pub = config.get("elasticsearch", "doc_type_pub")
    authors = get_author(formatName, es)
    completeText = ""
    author = authors["hits"]["hits"][0]
    list_pub = []
    completeText = ""
    if "pubs" in author["_source"] :
        for publication in author["_source"]["pubs"]:
            list_pub.append(publication["halId"])
            title_fr = publication["title_fr"]
            title_en = publication["title_en"]
            abstract_fr = ""
            abstract_en = ""
            if "abstract_fr" in publication :
                abstract_fr = publication["abstract_fr"]
            if "abstract_en" in publication :
                abstract_en = publication["abstract_en"]
            completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
            completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
        maintopic_data = {}
        data_list = []
        if completeText != "" :
            keywords = get_keywordsResearcher(list_pub, es)
            data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
        return data_list if data_list else ["empty"]
