# README #

### What is this repository for? ###

Crawl new publications from HAL and manage to add this publications to Elasticsearch.

Manage to add new publications to Individuals and Teams.

Create new topics for Individuals and Teams with the new publications.

### How do I get set up? ###

Use cron to run start.sh every 3 months.

- 30 03 01 Jan,Apr,Jul,Oct * /path/to/script